// "bitbucket.org/gotamer/gwu/messenger"
package messenger

import (
	"code.google.com/p/gowut/gwu"
)

const (
	MTYPE_CLEAR MsgType = iota
	MTYPE_ERROR
	MTYPE_GREEN
)

type MsgType int8

type message struct {
	mtype MsgType
	panel gwu.Panel
	Text  gwu.Label
}

func New(p gwu.Panel) message {
	var m message
	m.Text = gwu.NewLabel("")
	m.panel = p
	return m
}

func (m message) Clear(e gwu.Event) {
	m.mtype = MTYPE_CLEAR
	m.Text.SetText("")
	e.MarkDirty(m.panel)
}

func (m message) Error(e gwu.Event, msg string) {
	m.mtype = MTYPE_ERROR
	m.Text.SetText(msg)
	m.Text.Style().SetColor(gwu.CLR_RED)
	e.MarkDirty(m.panel)
}

func (m message) Success(e gwu.Event, msg string) {
	m.mtype = MTYPE_GREEN
	m.Text.SetText(msg)
	m.Text.Style().SetColor(gwu.CLR_GREEN)
	e.MarkDirty(m.panel)
}
