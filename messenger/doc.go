// "bitbucket.org/gotamer/gwu/messenger"
package messenger

/*

package main

import (
	"bitbucket.org/gotamer/gwu/messenger"
	"code.google.com/p/gowut/gwu"
)

func main() {
	win := gwu.NewWindow("main", "Task GUI Window")
	win.Style().SetFullWidth()
	win.SetHAlign(gwu.HA_CENTER)
	win.SetCellPadding(2)

	panel := gwu.NewPanel()
	panel.Add(gwu.NewLabel("Add a new Task:"))

	msg := messenger.New(panel)
	panel.Add(msg.Text)

	task := gwu.NewTextBox("")
	task.SetRows(5)
	task.SetCols(40)

	b := gwu.NewButton("OK")
	b.AddEHandlerFunc(func(e gwu.Event) {
		if task.Text() == "" {
			msg.Error(e, "msg error text")
		} else {
			p := gwu.NewPanel()
			text := gwu.NewLabel(task.Text())
			p.Add(text)

			b := gwu.NewButton("X")
			b.AddEHandlerFunc(func(e gwu.Event) {
				win.Remove(p)
				e.ReloadWin("main")
				win.SetFocusedCompId(task.Id())
			}, gwu.ETYPE_CLICK)
			p.Add(b)

			win.Add(p)
			msg.Success(e, "msg success text")
			task.SetText("")
			e.MarkDirty(task)
			e.ReloadWin("main")
			win.SetFocusedCompId(task.Id())
		}
	}, gwu.ETYPE_CLICK)

	panel.Add(task)
	panel.Add(b)
	win.Add(panel)
	win.SetFocusedCompId(task.Id())

	// Create and start a GUI server (omitting error check)
	server := gwu.NewServer("task", "localhost:8081")
	server.SetText("Task GUI")
	server.AddWin(win)
	server.Start("") // Also opens windows list in browser
}


*/
