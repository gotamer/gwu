gotamer/gwu/messenger
=====================

##### import "bitbucket.org/gotamer/gwu/messenger"

Messenger is a [Gowut] addon that extends gwu.Label to create error and success messages

See doc.go for example code.

[Gowut]: https://code.google.com/p/gowut
